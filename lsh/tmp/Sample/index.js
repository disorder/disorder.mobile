import {observer} from 'mobx-react/native';

import View from './view';

export const _NAME_ = observer(View);

export {default as _NAME_Model} from './model';
