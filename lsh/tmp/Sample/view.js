import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet,} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Button from 'react-native-button';

export default class _NAME_ extends Component {
  shouldComponentUpdate() {
    alert('!');
    return false;
  }

  render() {
    const model = this.props.model;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          _NAME_
        </Text>
        <Text>Counter: {model.counter}</Text>
        <Text>Total clicks: {model.total}</Text>
        <Button onPress={model.increase}>+</Button>
        <Button onPress={model.decrease}>-</Button>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
