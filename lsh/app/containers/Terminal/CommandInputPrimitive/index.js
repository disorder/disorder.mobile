import {observer} from 'mobx-react/native';
import React, {Component} from 'react';
import {
	View,
	TextInput,
	Text,
	TouchableOpacity,
	StyleSheet,
	Dimensions,
} from 'react-native';

const {width} = Dimensions.get('window');

const cmd = [
	'echo',
	'cat',
	'touch',
	'catc'
];

const exeCmd = [
	'camera',
	'clear',
	'ls',
]

@observer
export default class CommandInputPrimitive extends Component {

	componentDidMount() {}

	shouldComponentUpdate() {
		return false;
	}

	render() {
		const model = this.props.model;

		return (
      <View style={styles.inputContainer}>
        <TextInput
          blurOnSubmit={false}
          style={styles.input}
          onChangeText={model.updateCommand}
          onSubmitEditing={model.updateHistory}
          value={model.currentCmd}/>

        {cmd.map((item) => {
          return (
            <TouchableOpacity
              key={item}
              activeOpacity={0.9}
              delayPressOut={0}
              onPressOut={model
              .updateCommand
              .bind(model, item)}>
              <Text style={styles.cmdItem}>{item}</Text>
            </TouchableOpacity>
          )
        })}

				{exeCmd.map((item) => {
          return (
            <TouchableOpacity
              key={item}
              activeOpacity={0.9}
              delayPressOut={0}
              onPressOut={model
              .exeCommand
              .bind(model, item)}>
              <Text style={styles.cmdItem}>{item}</Text>
            </TouchableOpacity>
          )
        })}

        <TouchableOpacity activeOpacity={0.9}
				delayPressOut={0} onPressOut={model.updateHistory}>
          <Text style={styles.cmdItem}>Submit</Text>
        </TouchableOpacity>
      </View>
		)
	}
}

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
	input: {
		color: 'black',
		width,
		paddingLeft: 20
	},
	cmdItem: {
		flex: 0,
		textAlign: 'center',
		alignSelf: 'flex-start',
		color: 'white',
		margin: 9,
		paddingTop: 9,
		paddingBottom: 9,
		backgroundColor: '#333333',
		width: 81,
		borderRadius: 4.5
	}
});
