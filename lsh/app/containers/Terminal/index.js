import {observer} from 'mobx-react/native';

import View from './view';

export const Terminal = observer(View);

export {default as TerminalModel} from './model';
