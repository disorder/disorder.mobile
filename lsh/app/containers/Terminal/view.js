import {observer} from 'mobx-react/native';
import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
} from 'react-native';

import { ImmutableVirtualizedList } from 'react-native-immutable-list-view';

import Swiper from 'react-native-swiper';

import SnapCamera from 'components/SnapCamera';
import Status from 'components/Status';

import CommandInputPrimitive from './CommandInputPrimitive';

const {height, width} = Dimensions.get('window');

@observer
export default class Terminal extends Component {

	componentDidMount() {}

	shouldComponentUpdate() {
		return false;
	}

	generateRow =({item})=> <View style={styles.cmdWrapper}>
		<Status {...item} model={this.props.model} style={styles.command} />
	</View>

	render() {
		const model = this.props.model;

		return (
			<SnapCamera active={model.cameraActive} style={styles.container}>
				<View style={styles.terminal}>
					<ImmutableVirtualizedList
						ref={model.setListRef}
						style={styles.cmdList}
					  immutableData={model.displayStack}
					  renderItem={this.generateRow}
						keyExtractor={(item, index) => index}
					/>
				</View>

				<CommandInputPrimitive model={model} style={styles.inputContainer}/>

			</SnapCamera>
		)
	}
}

const textColor = '#C5C8C6';

const backgroundColor = 'white';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor
	},
	terminal: {
		flex: 0,
		justifyContent: 'center',
		alignItems: 'center',
		/*TODO: use the device height and subtract keyboard height for this*/
		height: height / 3,
		flexDirection: 'column-reverse',
		transform: [
			{scaleY: -1}
		]
	},
	cmdWrapper: {
		transform: [{ scaleY: -1 }]
	},
	cmdList: {
		flex: 1,
		alignSelf: 'flex-start',
		width
	},
	textinput: {
		flex: 0,
		borderBottomColor: 'white',
		borderBottomWidth: 1,
	}
});
