import {
  action,
  reaction,
  observable,
  observe,
  computed,
  autorun,
  runInAction
} from 'mobx';

import autobind from 'autobind-decorator';

import SnapCamera from 'components/SnapCamera';

import {List} from 'immutable';

import {infoLog, errorLog} from 'components/Status';

import LShell from 'components/LShell';

@autobind
class Model {

	listRef = null;

  @observable cameraActive = false;

  @observable currentCmd = "";

	@observable hash = Date.now();

  @observable history = [];

  @observable displayStack = List([]);

  constructor() {
    reaction(() => this.currentCmd, this.updateCommand);
  }

  async run(cmd) {
    const shell = new LShell(this);
    const output = await shell.run(cmd);

    if (!output) {
      return;
    }

    runInAction("Update display stack with shell output", () => {
      if (output.type === 'camera') {
        this.toggleCamera();
      } else {
				this.log(output);
      }
    })
  }

	setListRef(ref) {
		listRef = ref;
	}

  @action updateCommand(cmd) {
    this.currentCmd = cmd;
  }

  @action appendCommand(args) {
    this.currentCmd += ' ' + args;
  }

  @action exeCommand(cmd) {
    this.logHistory(cmd);
    this.history.push(cmd);

    this.run(cmd);
  }

  @action updateHistory() {
    const cmd = this.currentCmd;

    this.currentCmd = "";

    this.logHistory(cmd);

    if (cmd && cmd.length > 0) {
      this.history.push(cmd);
      this.run(cmd);
    } else {

    }
  }

  @action toggleCamera() {
    this.cameraActive = !this.cameraActive;
  }

  logHistory(cmd){
		this.log(infoLog(cmd));

		this.hash = Date.now();
    const shell = new LShell(this);
    shell.run(`echo '${cmd}' >> h`);
  }

  logError(cmd) {
		this.log(errorLog(cmd))

		this.hash = Date.now();
    const shell = new LShell(this);
    shell.run(`echo '${cmd}' >> e`);
  }

	@action log(displayObj) {
		// this.displayStack = this.displayStack.unshift(displayObj);
			this.displayStack = this.displayStack.unshift(displayObj);
	}
}

export default new Model();
