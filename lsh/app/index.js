import {useStrict} from 'mobx';
useStrict(true);

import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Router, Scene,} from 'react-native-mobx';
import {observer} from 'mobx-react/native';

import Swiper from 'react-native-swiper';

import SnapCamera from 'components/SnapCamera';

// view and model for Counter scene
import {Counter, CounterModel,} from 'containers/Counter';

import {Terminal, TerminalModel,} from 'containers/Terminal';

@observer
export default class App extends Component {
  render() {
    return (
      <View style={styles.terminal}>
        <Terminal model={TerminalModel}/>
      </View>
    )

    // return (
    //   <Swiper style={styles.wrapper} showsButtons={false} showsPagination={false}>
    //     <View style={styles.counter}>
    //       <Counter model={CounterModel}/>
    //     </View>
    //     <View style={styles.camera}>
    //       <SnapCamera />
    //     </View>
    //     <View style={styles.terminal}>
    //       <Terminal model={TerminalModel}/>
    //     </View>
    //   </Swiper>
    // )
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  terminal: {
    flex: 1
  },
  camera: {
    flex: 1
  },
  counter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }
})
