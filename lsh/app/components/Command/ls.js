import FileSystem from 'react-native-filesystem';

import {listLog, errorLog} from 'components/Status';

async function main({args, argv}){
  const path = argv._.length === 0
    ? "."
    : argv._[0].toString();

  const doesPathExist = await FileSystem.directoryExists(path);
  if (doesPathExist) {
    const fileList = await FileSystem.list(path);
    return listLog(fileList);
  } else {
    return errorLog(`${path} does not exist!`);
  }
}

export {main as default};
