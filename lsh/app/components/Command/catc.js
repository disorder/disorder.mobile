import FileSystem from 'react-native-filesystem';

import {listLog, errorLog} from 'components/Status';

async function main({argv, args}){
  if (argv._.length == 0) {
    return errorLog(`cat: missing file operand`);
  }
	const content = await FileSystem.readFile(argv._[0]);

	const lineList = content.split('\n');

	return listLog(lineList);
}

export {main as default};
