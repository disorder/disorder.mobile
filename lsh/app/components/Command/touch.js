import {errorLog} from 'components/Status';

import FileSystem from 'react-native-filesystem';

async function main({args, argv}){

  if (argv._.length == 0) {
    return errorLog(`touch: missing file operand`);
  }

  const path = argv._[0].toString();

  FileSystem.writeToFile(path, "");
}

export {main as default};
