import FileSystem from 'react-native-filesystem';

import {errorLog} from 'components/Status';

async function main({argv, args}){
  if (argv._.length == 0) {
    return errorLog(`cat: missing file operand`);
  }
  return FileSystem.readFile(argv._[0]);
}

export {main as default};
