import FileSystem from 'react-native-filesystem';

import {errorLog} from 'components/Status';

async function main({args, argv,}) {

  if (argv._.length == 0) {
    return errorLog(`rm: missing file operand`);
  }

  const path = argv._[0].toString();

  const isPathDirectory = await FileSystem.directoryExists(path);

  const isFileExist = await FileSystem.fileExists(path);

  if (isPathDirectory) {
    return errorLog(`${path} is a directory, use rmdir instead.`);
  }

  if (!isFileExist) {
    return errorLog(`${path} does not exist.`);
  }

  FileSystem.delete(path);
}

export {main as default};
