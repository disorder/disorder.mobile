export {default as ls} from './ls';

export {default as echo} from './echo';

export {default as rm} from './rm';

export {default as touch} from './touch';

export {default as clear} from './clear';

export {default as cat} from './cat';

export {default as camera} from './camera';

export {default as catc} from './catc';
