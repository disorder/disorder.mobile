import {successLog} from 'components/Status';

async function main({argv}) {
  // return cmd.slice(5);
  let out = "";

  for (let i = 0; i < argv._.length; i++) {
    if (i > 0) {
      out += " ";
    }
    if (argv._[i].op) {
      return out;
    }
    out += argv._[i];
  }
  return successLog(out);
}

export {main as default};
