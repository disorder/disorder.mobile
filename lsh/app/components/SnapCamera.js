import React, {Component} from 'react';
import {Dimensions, StyleSheet, Text, TouchableHighlight, View} from 'react-native';

import Camera from 'react-native-camera';

export default class SnapCamera extends Component {

  // <Text style={styles.capture} onPress={this.takePicture}>
  //   [CAPTURE]
  // </Text>
  render() {
    return this.props.active
    ? (
      <Camera ref={(cam) => {
        this.camera = cam;
      }}
      style={this.props.style}
      aspect={Camera.constants.Aspect.fill}>
        {this.props.children}
      </Camera>
    ) : (
      <View style={this.props.style}>
        {this.props.children}
      </View>
    );
  }

  takePicture = async() => {
    try {
      const data = await this.camera.capture();
      console.log(data);
    } catch (err) {
      console.error(err)
    }
  }
}
