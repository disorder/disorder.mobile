import {successLog, errorLog} from 'components/Status';

export function evalCode(cmd){
	const wrap = (
		cmd.indexOf('{') === 0 &&
		cmd.indexOf('}') === cmd.length - 1
	);

	// Evaluate code that was entered
	try {
		const output = eval.call(null, (wrap ? '(' : '') + cmd + (wrap ? ')' : ''));

		const result = (typeof output === 'object')
			? JSON.stringify(output)
			: output;

		return successLog(result);
	} catch(err) {
		return errorLog(err.toString());
	}
}
