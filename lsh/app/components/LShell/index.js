import FileSystem from 'react-native-filesystem';

import {parse as ShellParser} from 'shell-quote';

import ParseArgs from 'minimist';

import {successLog, errorLog} from 'components/Status';

import * as Command from 'components/Command';

import {evalCode} from './utils';

export default class LShell {

	constructor(model) {
		this.model = model;
	}

	redirect(output, arg, i) {
		if (typeof arg[i+1] === 'string') {
			FileSystem.writeToFile(arg[i+1], output);
		} else if (arg[i+1].op === '>') {
			FileSystem.writeToFile(arg[i+2], `\n${output}`, true);
		} else {
			this.model.logError(JSON.stringify({output, arg, i}))
		}
	}

	pipe(output, arg, i) {

	}

	parse(cmd) {

		const cmdArray = ShellParser(cmd);

		const main = cmdArray[0];

		const args = cmdArray.slice(1);

		// TODO: Need to replace this with a more reliable parser
		const argv = ParseArgs(args);

		return {
			main,
			args,
			argv,
		};
	}

	async run(cmd) {
		const {
			main,
			args,
			argv,
		} = this.parse(cmd);

		try {
			if(!Command[main]) {
				return evalCode(cmd);
			}

			const result = await Command[main]({cmd, args, argv});

			const output = (typeof result === 'string')
				? successLog(result)
				: result;

			// return successLog(JSON.stringify(argv));

			argv._.map((arg, i) => {
				if (!arg.op) return;
				switch (arg.op) {
					case '>':
						if (argv._[i-1].op !== '>') {
							this.redirect(output.value, argv._, i);
						}
						break;
					default:
				}
			})

			return output;
		} catch (e) {
			return errorLog(e.toString())
		}
	}
}
