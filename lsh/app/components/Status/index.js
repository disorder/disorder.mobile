import React from 'react'

import {
  View,
  TouchableOpacity,
  Text,
	StyleSheet
} from 'react-native';

import Emoji from 'react-native-emoji';

const Status = (props) => {
  const {timestamp, type, value, model} = props;

  switch (type) {
    case 'list': {
      return (
        <View key={timestamp} style={styles.buttonContainer}>
          {
            value.map((v, i) =>
            <TouchableOpacity
              key={i}
              onPress={model.appendCommand.bind(model, v)}>
              <Text style={styles.button}><Emoji name="heart"/> {v}</Text>
          	</TouchableOpacity>)
          }
      </View>
      )
    }
    case 'info':
      return (<Text key={timestamp} style={[styles.text, {color: 'blue'}]}>
				<Emoji name="dollar"/> {value}
			</Text>);
    case 'error':
      return (<Text key={timestamp} style={[styles.text, {color: 'red'}]}>
				<Emoji name="hand"/> {value}
			</Text>);
    case 'success':
    default:
      return (<Text key={timestamp} style={[styles.text, {color: 'green'}]}>
				<Emoji name="heart"/> {value}
			</Text>);
  }
}

const styles = StyleSheet.create({
	text: {
		marginBottom: 10
	},
	buttonContainer: {
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		justifyContent: 'center'
	},
	button:{
		flex: 0,
		textAlign: 'center',
		alignSelf: 'flex-start',
		color: 'white',
		margin: 9,
		paddingTop: 9,
		paddingBottom: 9,
		backgroundColor: '#333333',
		width: 81,
		borderRadius: 4.5
	}
})

export default Status

export function listLog(value) {
  return {
		timestamp: Date.now(),
    err: false,
    type: 'list',
    value
  }
};

export function infoLog(value) {
  return {
		timestamp: Date.now(),
    err: false,
    type: 'info',
    value
  }
}

export function successLog(value) {
  return {
		timestamp: Date.now(),
    err: false,
    type: 'success',
    value
  }
}

export function errorLog(value) {
  return {
		timestamp: Date.now(),
    err: true,
    type: 'error',
    value
  }
}
